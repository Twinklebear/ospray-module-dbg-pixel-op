#pragma once

#include <ospray/fb/PixelOp.h>

using namespace ospray;

struct DebugPixelOp : public PixelOp {
  struct Instance : public PixelOp::Instance {
    Instance();
    void postAccum(Tile &tile) override;
    std::string toString() const override;
  };
  PixelOp::Instance* createInstance(FrameBuffer *fb, PixelOp::Instance *prev) override;
};

