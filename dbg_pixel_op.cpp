#include <array>
#include <cstdio>
#include "dbg_pixel_op.h"

#pragma pack(1)
struct BMPHeader {
  std::array<uint8_t, 2> header;
  uint32_t file_size;
  // 4 reserved bytes we don't care about
  uint32_t dont_care = 0;
  // Offset in the file to the pixel array
  uint32_t px_array = 54;
  uint32_t header_size = 40;
  std::array<int32_t, 2> dims;
  uint16_t color_planes = 1;
  uint16_t bpp = 24;
  uint32_t compression = 0;
  uint32_t img_size;
  std::array<int32_t, 2> res;
  uint32_t color_palette = 0;
  uint32_t important_colors = 0;

  BMPHeader(uint32_t img_size, int32_t w, int32_t h)
    : header({'B', 'M'}), file_size(54 + img_size), dims({w, h}), img_size(img_size),
    res({2835, 2835})
  {}
};

float convert_srgb(const float x) {
  if (x < 0.0031308) {
    return 12.92 * x;
  } else {
    return 1.055 * std::pow(x, 1.f / 2.4f) - 0.055;
  }
}

DebugPixelOp::Instance::Instance() {}
void DebugPixelOp::Instance::postAccum(Tile &tile) {
  const int tile_x = tile.region.lower.x / TILE_SIZE;
  const int tile_y = (tile.fbSize.y - tile.region.upper.y) / TILE_SIZE;
  const int tile_id = tile_x + tile_y * (tile.fbSize.x / TILE_SIZE);
  const std::string file = "dbg_tile_out_tile_" + std::to_string(tile_id) + ".bmp";
  FILE *fp = fopen(file.c_str(), "wb");
  if (!fp){
    std::cerr << toString() << "postAccum Error: failed to open file "
      << file << std::endl;
  }
  uint32_t w = TILE_SIZE, h = TILE_SIZE;
  BMPHeader bmp_header{3 * w * h, static_cast<int32_t>(w),
    static_cast<int32_t>(h)};
  if (fwrite(&bmp_header, sizeof(BMPHeader), 1, fp) != 1){
    fclose(fp);
    std::cerr << toString() << "postAccum Error: failed to write header" << std::endl;
  }
  // Convert to BGR8 color for BMP
  std::vector<uint8_t> data(w * h * 3, 0);
  size_t pixelID = 0;
  for (size_t iy = 0; iy < h; ++iy) {
    for (size_t ix = 0; ix < w; ++ix, ++pixelID) {
      data[pixelID * 3] = 255.f * convert_srgb(clamp(tile.b[pixelID], 0.f, 1.f));
      data[pixelID * 3 + 1] = 255.f * convert_srgb(clamp(tile.g[pixelID], 0.f, 1.f));
      data[pixelID * 3 + 2] = 255.f * convert_srgb(clamp(tile.r[pixelID], 0.f, 1.f));
    }
  }

  // Write each row follwed by any necessary padding
  uint32_t padding = (w * 3) % 4;
  for (uint32_t r = 0; r < h; ++r){
    if (fwrite(data.data() + 3 * w * r, 1, 3 * w, fp) != 3 * w){
      fclose(fp);
      std::cerr << toString() << "postAccum Error: failed to write scanline" << std::endl;
    }
    if (padding != 0){
      if (fwrite(data.data(), 1, padding, fp) != padding){
        fclose(fp);
        std::cerr << toString() << "postAccum Error: failed to write padding" << std::endl;
      }
    }

  }
  fclose(fp);
}
std::string DebugPixelOp::Instance::toString() const {
  return "DebugPixelOp::Instance";
}
PixelOp::Instance* DebugPixelOp::createInstance(FrameBuffer *fb, PixelOp::Instance *prev) {
  return new Instance;
}

OSP_REGISTER_PIXEL_OP(DebugPixelOp, dbg_pixel_op); 

extern "C" void ospray_init_module_dbg_pixel_op() 
{
  std::cout << "#osp:dbg_pixel_op: loading 'dbg_pixel_op' module" << std::endl;
}

