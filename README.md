# ospray-module-dbg-pixel-op

Defines a `PixelOp` `dbg_pixel_op` which simply saves tiles sent to the
op out as BMPs, useful for debugging pixel operation issues. To re-assemble the
framebuffer from the saved tiles you can use ImageMagick's montage tool:

```bash
montage `ls *.bmp | sort -V` -geometry +0+0 -tile MxN out.jpg
```

Where `M` and `N` are the number of tiles along X and Y respectively in the framebuffer.

